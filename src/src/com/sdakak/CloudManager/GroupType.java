package src.com.sdakak.CloudManager;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import src.com.sdakak.CloudManager.Constants.Size;

public class GroupType {
	private final String id;
	private String description;
	private String type;
	private Size size;
	private int numHosts;
	private boolean eager;
	private List<String> dependsOn;

	@JsonCreator
	public GroupType(@JsonProperty("id") String id,
			@JsonProperty("description") String description,
			@JsonProperty("type") String type, @JsonProperty("size") Size size,
			@JsonProperty("numHosts") int numHosts,
			@JsonProperty("eager") boolean eager,
			@JsonProperty("dependsOn") List<String> dependsOn) {

		this.id = id;
		this.description = description;
		this.type = type;
		this.size = size;
		this.numHosts = numHosts;
		this.eager = eager;
		this.dependsOn = dependsOn;
	}
	
	public GroupType (String description, String type, int size, int numHosts, int eager, String dependsOn) {
		this.id = UUID.randomUUID().toString();
		this.description = description;
		this.type = type;
		this.size = Size.values()[size];
		this.numHosts = numHosts;
		this.eager = (eager == 1);
		this.dependsOn = Arrays.asList(dependsOn.trim().split(","));
	}

	public String getId() {
		return id;
	}

	public String getType() {
		return type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Size getSize() {
		return size;
	}

	public int getNumHosts() {
		return numHosts;
	}

	public void setNumHosts(int numHosts) {
		this.numHosts = numHosts;
	}

	public boolean isEager() {
		return eager;
	}

	public void setEager(boolean eager) {
		this.eager = eager;
	}
	
	public void setDependsOn(String dependsOn) {
		this.dependsOn = Arrays.asList(dependsOn.trim().split(","));
	}

	public List<String> getDependsOn() {
		return dependsOn;
	}

	public boolean dependsOn(String id) {
		if (dependsOn.contains(id)) {
			return true;
		}
		return false;
	}

	public void addDependency(String id) {
		if (!dependsOn.contains(id))
			dependsOn.add(id);
	}

	public void removeDependency(String id) {
		if (dependsOn.contains(id)) {
			dependsOn.remove(id);
		}
	}
	
	public void setSize(int size) {
		this.size = Size.values()[size];
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public void setEagerInt(int eager) {
		this.eager = (eager == 1);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GroupType other = (GroupType) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "GroupType [id=" + id + ", description=" + description + ", type="
				+ type + ", size=" + size + ", numHosts=" + numHosts + ", eager="
				+ eager + ", dependsOn=" + dependsOn + "]";
	}
	
	

}
