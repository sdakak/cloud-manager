package src.com.sdakak.CloudManager;

import java.util.UUID;
import src.com.sdakak.CloudManager.Constants.State;

public class Host {
	private final String id;
	private String description;
	private String type;
	private boolean active;
	private String groupID;
	private State state;
	
//	public Host (String description, String type, int size) {
//		this.id = UUID.randomUUID().toString();
//		this.description = description;
//		this.type = type;
//	}
	
	public Host (HostType ht) {
		this.id = UUID.randomUUID().toString();
		this.description = ht.getDescription();
		this.type = ht.getType();
		this.state = State.CREATED;
	}
	
	public boolean isActive() {
		return active;
	}
	
	public synchronized void boot() {
		if (state == State.CREATED || state == State.SHUTDOWN) {
			this.state = State.BOOTING;
			bootme();
			this.state = State.RUNNING;
			this.active = true;
		}
	}
	
	public synchronized void shutdown() {
		if (state != State.CREATED) {
			this.state = State.SHUTTING_DOWN;
			shutmedown();
			this.state = State.SHUTDOWN;
			this.active = false;
		}
	}

	private void bootme() {
		// make sure this operation doesn't hang the entire CloudManager
		// Lots of stuff to boot machine
	}
	
	private void shutmedown() {
		// Lots of stuff to shut machine down cleanly
	}

	public void setActive(boolean active) {
		this.active = active;
		if (active)
			boot();
	}
	
	public State getState() {
		return this.state;
	}

	
	public String getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Host [id=" + id + ", description=" + description + ", type=" + type
				+ ", active=" + active + ", groupID=" + groupID + ", state=" + state
				+ "]";
	}
	
	
}
