package src.com.sdakak.CloudManager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import src.com.sdakak.CloudManager.Constants.State;
import asg.cliche.Command;
import asg.cliche.Param;
import asg.cliche.ShellFactory;

public class CloudManager {
	List<String> services;
	List<HostType> hostTypes;
	List<String> clusters;
	List<GroupType> groupTypes;
	List<Host> inactiveHosts;
	List<Host> activeHosts;
	
	IO io;

	public CloudManager() throws Exception {
		this.io = new IO();
		
		this.hostTypes = io.getHostTypes();
		this.services = io.getServices();
		this.groupTypes = io.getGroupTypes();
		this.clusters = io.getClusters();
		this.inactiveHosts = new ArrayList<Host>();
		this.activeHosts = new ArrayList<Host>();
		
	}

	@Command(description = "Add new hosts by HostType ID")
	public void addHost (String htID) {
		HostType required = null;
		for (HostType ht : hostTypes) {
			if (ht.getID().equals(htID))
				required = ht;
		}
		if (required != null) {
			Host h = new Host(required);
			inactiveHosts.add(h);
		}
		update();
	}
	
	@Command(description = "Bring host with specified id into load balancer")
	public void activateHost(String ID) {
		for (Host h : inactiveHosts) {
			if(h.getId().equals(ID)) {
				h.setActive(true);
			}
		}
		update();
	}
	
	@Command(description = "Make host with specified id out of load balancer")
	public void deactivateHost(String ID) {
		for (Host h : activeHosts) {
			if(h.getId().equals(ID)) {
				h.setActive(false);
			}
		}
		update();
	}
	
	public void update() {
		Iterator<Host> iter = inactiveHosts.iterator();
		while (iter.hasNext()) {
			Host h = iter.next();
			if (h.getState() == State.RUNNING && h.isActive()) {
				iter.remove();
				activeHosts.add(h);
			}
		}
		
		iter = activeHosts.iterator();
		while (iter.hasNext()) {
			Host h = iter.next();
			if (!h.isActive()) {
				iter.remove();
				inactiveHosts.add(h);
			}
		}
	}
	
	@Command(description = "Update the service with the specified id with the specified params")
	public void updateHostTypes(@Param(name = "id") String id,
			@Param(name = "description") String description,
			@Param(name = "type") String type, @Param(name = "size") int size) {
		if (services.contains(type)) {
			for (HostType ht : hostTypes) {
				if (ht.getID().equals(id)) {
					ht.setDescription(description);
					ht.setType(type);
					ht.setSize(size);
				}
			}
		}
	}
	
	

	@Command(description = "Add new type of service if we already don't have it")
	public void addHostType(@Param(name = "description") String description,
			@Param(name = "type") String type, @Param(name = "size") int size) {
		if (!services.contains(type)) {
			HostType ht = new HostType(description, type, size);
			hostTypes.add(ht);
		}
		services.add(type);
	}

	@Command(description = "Update the cluster with the specified id with the specified params")
	public void updateGroupTypes(@Param(name = "id") String id,
			@Param(name = "description") String description,
			@Param(name = "type") String type, @Param(name = "size") int size, @Param(name = "numHosts") int numHosts, @Param(name = "eager") int eager,
			@Param(name = "dependsOn") String dependsOn) {
		if (clusters.contains(type)) {
			for (GroupType gt : groupTypes) {
				if (gt.getId().equals(id)) {
					gt.setDescription(description);
					gt.setType(type);
					gt.setSize(size);
					gt.setNumHosts(numHosts);
					gt.setEagerInt(eager);
					gt.setDependsOn(dependsOn);
				}
			}
		}
	}

	@Command(description = "Add new type of cluster if we already don't have it")
	public void addGroupTypes(@Param(name = "description") String description, @Param(name = "type") String type, @Param(name = "size") int size,
			@Param(name = "numHosts") int numHosts, @Param(name = "eager") int eager, @Param(name = "dependsOn") String dependsOn) {
		if (!clusters.contains(type)) {
			GroupType gt = new GroupType(description, type, size, numHosts, eager,
					dependsOn);
			groupTypes.add(gt);
		}
		clusters.add(type);
	}
	
	@Command(description = "Print all HostTypes")
	public void printHostTypes() {
		for (HostType ht: hostTypes) {
			System.out.println(ht);
		}
	}
	
	@Command(description = "Print all GroupTypes")
	public void printGroupTypes() {
		for (GroupType gt : groupTypes) {
			System.out.println(gt);
		}
		
	}
	
	@Command(description = "Print active hosts")
	public void printActiveHosts() {
		for (Host h : activeHosts) {
			System.out.println(h);
		}
		
	}
	
	@Command(description = "Print inactive hosts")
	public void printInactiveHosts() {
		for (Host h : inactiveHosts) {
			System.out.println(h);
		}
		
	}
	
	@Command(description = "Update the json files and shutdown everything")
	public void shutDown() throws Exception {
		io.shutDown();
	}
	
	public static void main(String args[]) throws Exception {
		ShellFactory.createConsoleShell("CloudManager", "Cloud Manager",
				new CloudManager()).commandLoop();
	}

}
