package src.com.sdakak.CloudManager;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

import src.com.sdakak.CloudManager.Constants.Size;

public class HostType {
	private final String id;
	private String description;
	private String type;
	private Size size;

	@JsonCreator
	public HostType(@JsonProperty("id") String id,
			@JsonProperty("description") String description,
			@JsonProperty("type") String type, @JsonProperty("size") Size size) {
		this.id = id;
		this.description = description;
		this.type = type;
		this.size = size;
	}
	
	public HostType(String description, String type, int size) {
		this.id = UUID.randomUUID().toString();
		this.description = description;
		this.type = type;
		this.size = Size.values()[size];
	}

	public String getID() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public Size getSize() {
		return size;
	}
	
	public void setSize(int size) {
		this.size = Size.values()[size];
	}
	
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HostType other = (HostType) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HostType [id=" + id + ", description=" + description + ", type="
				+ type + ", size=" + size + "]";
	}
	
	

}
