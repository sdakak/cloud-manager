package src.com.sdakak.CloudManager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class IO {
	ObjectMapper mapper;
	List<HostType> hostTypes;
	List<String> services; // Host.Type can only be one of these
	List<GroupType> groupTypes;
	List<String> clusters; // Group.Type can only be one of these

	public IO() throws Exception {
		mapper = new ObjectMapper();
		services = new ArrayList<String>();
		clusters = new ArrayList<String>();

		hostTypes = mapper.readValue(new File(Constants.hostTypeFile),
				new TypeReference<List<HostType>>() {
				});
		
		populateServicesAndValidateHostType();
		groupTypes = mapper.readValue(new File(Constants.groupTypeFile),
				new TypeReference<List<GroupType>>() {
				});
		
		populateClustersAndValidateGroupType();
		//hostTypes.get(0).setDescription("I have been changed. Forever");
		//mapper.writeValue(new File("resources/modified.json"), hostTypes);
	}
	
	private void readHosts() {
		
	}
	
	private void readGroups() {
		
	}

	public void populateServicesAndValidateHostType() throws Exception {
		for (HostType ht : hostTypes) {
			String service = ht.getType();
			if (services.contains(service)) {
				throw new Exception(
						"Service type needs to be unique. HostType json file has duplicates");
			}
			services.add(service);
		}
	}

	public void populateClustersAndValidateGroupType() throws Exception {
		for (GroupType gt : groupTypes) {
			String cluster = gt.getType();
			if (clusters.contains(cluster)) {
				throw new Exception(
						"Cluster type needs to match one of GroupTypes. GroupType json file has an unknown service");
			}
			clusters.add(cluster);
		}
	}

	public List<HostType> getHostTypes() {
		return hostTypes;
	}

	public List<String> getServices() {
		return services;
	}
	
	public List<String> getClusters() {
		return clusters;
	}

	public List<GroupType> getGroupTypes() {
		return groupTypes;
	}
	
	public void writeHostTypes() throws Exception {
		mapper.writeValue(new File(Constants.hostTypeFile), hostTypes); 
	}
	
	public void writeGroupTypes() throws Exception {
		mapper.writeValue(new File(Constants.groupTypeFile), groupTypes);
	}
	
	public void shutDown() throws Exception {
		writeHostTypes();
		writeGroupTypes();
	}
	

}
