package src.com.sdakak.CloudManager;

public class Constants {
	public static enum State {
		CREATED, BOOTING, RUNNING, SHUTTING_DOWN, SHUTDOWN
	}

	public static enum Size {
		SMALL, MEDIUM, LARGE
	}

	public static String hostTypeFile = "resources/HostType.json";
	public static String groupTypeFile = "resources/GroupType.json";

}
